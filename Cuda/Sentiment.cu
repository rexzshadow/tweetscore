/*
Created by Rex Zehao Pan
*/

#include "stdio.h" 
#include "math.h"
#include "stdlib.h"
#include "string.h"
#include <time.h>
#include <sys/time.h>
#include <cuda.h>

__global__
void gpu_getscore(char char **d_score, char **d_scorenum, int d_size, char *d_word, int d_result);

int get_score(char **score, char ** scorenum, int size, char *word);
unsigned charTOunsigned(const char * c);
int charTOint(const char * c);
void tweet_score(char **score, int *scorenum, int scoresize, char **tweets, int tweetsize, int BLOCKSIZE);

int main(int argc, char** argv)
{
	cudaSetDevice(2);
	struct timeval start, end;
	FILE *file = fopen ( "words.txt", "r" );
	char *score[2477];
	char *scorenum[2477];
	int scores[2477];
	int scoresize = 2477;
	char *tweets[53159];
	int result[53159];
	int tweetsize =  53159;
	int count = 0;
	char line[128];
	int BLOCKSIZE = atoi(argv[1]);


	if ( file != NULL ) 
	{
		while ( fgets ( line, sizeof(line), file ) != NULL ) /* read a line */
		{
			score[count] = (char*) malloc(sizeof(char) * 128);
			strncpy(score[count], line, 128);
			count ++;
		}
		
		fclose ( file );
	}
	
	FILE *file_1 = fopen ( "score.txt", "r" );
	count = 0;
	if ( file != NULL ) 
	{
		while ( fgets ( line, sizeof(line), file_1 ) != NULL ) /* read a line */
		{
			line[strcspn(line, "\n")] = '\0';
			scores[count] = charTOint(line);
			count ++;
		}
		
		fclose ( file_1 );
	}
	
	
	int countdata = 0;
	FILE *file_tweet = fopen ( "data.txt", "r" );
	
	if ( file_tweet != NULL ) 
	{

		while ( fgets ( line, sizeof(line), file_tweet ) != NULL ) 
		{
			
			tweets[countdata] = (char*) malloc(sizeof(char) * 128);
			strncpy(tweets[countdata], line, 128);
			countdata ++;
		}
		
		fclose(file_tweet);
	}


	gettimeofday(&start, NULL);
	tweet_score(score,scorenum, scoresize, tweets, tweetsize, BLOCKSIZE);
	gettimeofday(&end, NULL);
	printf("%ld\n", ((end.tv_sec * 1000000 + end.tv_usec)
- (start.tv_sec * 1000000 + start.tv_usec)));
	return 0;
}

__global__
void gpu_getscore(char **d_score, int *d_scorenum, int d_size, char *d_word, int d_result)
{
	int lix = threadIdx.x;
	int liy = threadIdx.y;
	int glix=lix+blockIdx.x*blockDim.x;
	int gliy=liy+blockIdx.y*blockDim.y;
	int step = d_size/blockDim.x + 1;
	int newi;
	int globalX = glix * d_size + gliy;
	
	if(globalX == 0)
	{
		d_score = 0;
	}
	__shared__ int scores[2477];
	for(int i = 0; i < d_size;i++)
	{
		scores[i] = d_scorenum[i];
	}
	for(int i = 0; i < step; i++)
	{
		newi = i*step;
		if(newi < d_size)
		{
			if(strcmp(d_score[newi], d_word) == 0)
			{
				d_score = scores[newi];
			}
		}
	}
}

void tweet_score(char **score, int *scorenum , int scoresize, char **tweets, int tweetsize, int BLOCKSIZE)
{
	dim3 dimBlock (BLOCKSIZE, BLOCKSIZE, 1);
	dim3 dimGrid(scoresize/BLOCKSIZE, scoresize/BLOCKSIZE,1);
	char *p;
	char *temp;
	char input[128];
	int points = 0;
	int value;
	char *tweet[144];
	int count = 0;
	int result[tweetsize];
	char **d_score;
	int *d_scorenum;
	char *d_word;
	int *d_result;
	cudaMalloc((void **)&d_scorenum, sizeof(int));
	cudaMalloc((void **)&d_score, sizeof(char)*scoresize*128);
	cudaMalloc((void **)&d_scorenum, sizeof(int)*scoresize);
	cudaMalloc((void **)&d_word, sizeof(char)*128);
	cudaMemcpy(d_score, score, sizeof(char)*scoresize*128, cudaMemcpyHostToDevice);
	cudaMemcpy(d_scorenum, scorenum, sizeof(int)*scoresize, cudaMemcpyHostToDevice);
	
	for (int i = 0; i < tweetsize; i++)
	{
		count = 0;
		temp = tweets[i];
		temp[strcspn(temp, "\n")] = '\0';
		p = strtok(temp, " ");
		
		while (p != NULL)
		{
			tweet[count] = (char*) malloc(sizeof(char) * 128);
			strncpy(tweet[count], p, sizeof(p));
			count ++;
			p = strtok (NULL, " ");
		}

		for (int j =0; j < count; j++)
		{

			//value = get_score(score,scorenum, scoresize, tweet[j], t);
			//void gpu_getscore(char char **d_score, char **d_scorenum, int d_size, char *d_word, int d_result);
			cudaMemcpy(d_word, tweet[j], sizeof(char)*128, cudaMemcpyHostToDevice);
			gpu_getscore<<<dimGrid,dimBlock>>>(d_score, d_scorenum, scoresize, d_word, d_result);
			cudaMemcpy(value, d_result, sizeof(int), cudaMemcpyDeviceToHost);
			points = points + value;
		}
		result[i] = points;
		points = 0;
		count = 0;
		//free(tweet);
	}
	
	FILE *file = fopen ("results.txt", "w");
	for(int i = 0; i < tweetsize; i ++)
	{
		fprintf(file, "%d\n", result[i]);
	}
	
	fclose(file);
}

int get_score(char **score, char ** scorenum, int size, char *word)
{

	char name[50];
	char value[50];
	int result = 0;
	//printf("start get score\n");
	//printf(word);
	//printf("\n");
	
	for(int i = 0; i < size; i ++)
	{	
		strncpy(name, score[i], sizeof(score[i]));
		name[strcspn(name, "\n")] = '\0';
		if(strcmp(name, word) == 0)
		{
			//printf("match \n");
			
			strncpy(value, scorenum[i], sizeof(scorenum[i]));

			value[strcspn(value, "\n")] = '\0';

			result = charTOint(value);
		}
	}

	return result;
}

unsigned charTOunsigned(const char * c) {
    char p = *c;
    unsigned ergebnis = 0;
    while (p) {
        ergebnis = ergebnis * 10 + (p - '0');
        c++;
        p = *c;
    }
    return ergebnis;
}

int charTOint(const char * c) {
  return (*c == '-') ? -charTOunsigned(c+ 1) : charTOunsigned(c);
}
