/*
Created by Rex Zehao Pan
*/

#include "stdio.h" 
#include "math.h"
#include "stdlib.h"
#include "string.h"
#include <time.h>
#include <sys/time.h>
#include <omp.h>

int get_score(char **score, char ** scorenum, int size, char *word, int t);
unsigned charTOunsigned(const char * c);
int charTOint(const char * c);
void tweet_score(char **score, char ** scorenum, int scoresize, char **tweets, int tweetsize, int t);

int main(int argc, char** argv)
{
	struct timeval start, end;
	FILE *file = fopen ( "words.txt", "r" );
	char *score[2477];
	char *scorenum[2477];
	int scoresize = 2477;
	char *tweets[53159];
	int result[53159];
	int tweetsize =  53159;
	int count = 0;
	char line[128];
	int t = atoi(argv[1]);;
	if ( file != NULL ) 
	{
		while ( fgets ( line, sizeof(line), file ) != NULL ) /* read a line */
		{
			score[count] = (char*) malloc(sizeof(char) * 128);
			strncpy(score[count], line, 128);
			count ++;
		}
		
		fclose ( file );
	}
	
	FILE *file_1 = fopen ( "score.txt", "r" );
	count = 0;
	if ( file != NULL ) 
	{
		while ( fgets ( line, sizeof(line), file_1 ) != NULL ) /* read a line */
		{
			scorenum[count] = (char*) malloc(sizeof(char) * 128);
			strncpy(scorenum[count], line, 128);
			count ++;
		}
		
		fclose ( file_1 );
	}
	
	
	int countdata = 0;
	FILE *file_tweet = fopen ( "data.txt", "r" );
	
	if ( file_tweet != NULL ) 
	{

		while ( fgets ( line, sizeof(line), file_tweet ) != NULL ) 
		{
			
			tweets[countdata] = (char*) malloc(sizeof(char) * 128);
			strncpy(tweets[countdata], line, 128);
			countdata ++;
		}
		
		fclose(file_tweet);
	}

	
	gettimeofday(&start, NULL);
	tweet_score(score,scorenum, scoresize, tweets, tweetsize, t);
	gettimeofday(&end, NULL);
	printf("%ld\n", ((end.tv_sec * 1000000 + end.tv_usec)
- (start.tv_sec * 1000000 + start.tv_usec)));
	return 0;
}

void tweet_score(char **score, char ** scorenum , int scoresize, char **tweets, int tweetsize, int t)
{
	char *p;
	char *temp;
	char input[128];
	int points = 0;
	int value;
	char *tweet[144];
	int count = 0;
	int result[tweetsize];
	
	omp_set_num_threads(t);
	#pragma omp parallel for private(count, temp, p, value, points)
	for (int i = 0; i < tweetsize; i++)
	{
		p = NULL;
		count = 0;
		points = 0;
		temp = tweets[i];
		temp[strcspn(temp, "\n")] = '\0';
		p = strtok(temp, " ");
		
		while (p != NULL)
		{
			tweet[count] = (char*) malloc(sizeof(char) * 128);
			strncpy(tweet[count], p, sizeof(p));
			count ++;
			p = strtok (NULL, " ");
		}

		for (int j =0; j < count; j++)
		{
			value = get_score(score,scorenum, scoresize, tweet[j], t);
			points = points + value;
		}
		result[i] = points;
	}
	
	FILE *file = fopen ("results.txt", "w");
	for(int i = 0; i < tweetsize; i ++)
	{
		fprintf(file, "%d\n", result[i]);
	}
	
	fclose(file);
}

int get_score(char **score, char ** scorenum, int size, char *word, int t)
{

	char name[50];
	char value[50];
	int result = 0;

	
	for(int i = 0; i < size; i ++)
	{	
		strncpy(name, score[i], sizeof(score[i]));
		name[strcspn(name, "\n")] = '\0';
		if(strcmp(name, word) == 0)
		{
			strncpy(value, scorenum[i], sizeof(scorenum[i]));
			value[strcspn(value, "\n")] = '\0';
			result = charTOint(value);
		}
	}

	return result;
}

unsigned charTOunsigned(const char * c) {
    char p = *c;
    unsigned ergebnis = 0;
    while (p) {
        ergebnis = ergebnis * 10 + (p - '0');
        c++;
        p = *c;
    }
    return ergebnis;
}

int charTOint(const char * c) {
  return (*c == '-') ? -charTOunsigned(c+ 1) : charTOunsigned(c);
}
