#!/bin/bash
#SBATCH --partition=compute   ### Partition
#SBATCH --job-name=Project02 ### Job Name
#SBATCH --time=00:10:00     ### WallTime
#SBATCH --nodes=1           ### Number of Nodes
#SBATCH --ntasks-per-node=1 ### Number of tasks (MPI processes)
##SBATCH --cpus-per-task=2  ### Number of threads per task (OMP threads). Get num threads using program arguments


for((j=2; j<=32; j = j* 2))
do
	echo $j
	for((i=0;i<10;i++))
	do
		echo $i
		srun ./Sentiment $j >>runs.csv
	done
done

